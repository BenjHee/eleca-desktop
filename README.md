# Eleca - Desktop

Eleca is electronic developpement companion app. By defining a system with resistor capacitor or inductor, Eleca compute thebest components for any given system defined with these component.

Eleca works by testing every possible combination of component for a given system, and listing the best results.

## What's new

*new feature will be added in this section*

## Features

- Voltage divider

- User defined systems

## Screenshots

<img src="assets/screenshots/Screenshot_01.png" width=50%><img src="assets/screenshots/Screenshot_02.png" width=50%>

<img src="assets/screenshots/Screenshot_03.png" width=50%><img src="assets/screenshots/Screenshot_04.png" width=50%>

## TODO

- Operational amplifier

- Filters

- Translations

## Download

*The app is not currenly available as a stable realease, however the app can be buid using the source*

## Dependencies and library

- [Electron](https://electronjs.org/) : Build cross-platform desktop apps with JavaScript, HTML, and CSS
- [Expr-eval](https://silentmatt.com/javascript-expression-evaluator/) : Parses and evaluates mathematical expressions.
- [Bulma css](https://bulma.io/) : Bulma is a modern CSS framework based on [Flexbox](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Using_CSS_flexible_boxes).
- [Spectre css](https://picturepan2.github.io/spectre/) : A lightweight, responsive and modern CSS framework.

## FAQ
