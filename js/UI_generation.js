'use-strict'

function HTML_CreateSideBar() {

    let items = [{
            title: 'General',
            element: [{
                    title: "Pont Diviseur",
                    link: 'pontDiviseur.html'
                },
                {
                    title: "Fonction Utilisateur",
                    link: 'customFunction.html'
                }
            ]
        },
        {
            title: "AOP",
            element: [{
                title: "Differential amplifier",
                link: 'differentialAmplifier.html'
            }, {
                title: "Inverting amplifier",
                link: 'invertingAmplifier.html'
            }, {
                title: "Non-Inverting amplifier",
                link: 'nonInvertingAmplifier.html'
            }, {
                title: "Summing amplifier",
                link: 'summingAmplifier.html'
            }]
        },
        {
            title: "Filters",
            element: [{
                title: "Low-Pass filter",
                link: 'lowPassFilter.html'
            }, {
                title: "High-Pass filter",
                link: 'highPassFilter.html'
            }, {
                title: "Band-Pass filter",
                link: 'bandPassFilter.html'
            }]
        }
    ]

    let mainDiv = document.createElement('div');
    mainDiv.classList.add('docs-sidebar', 'off-canvas-sidebar');
    mainDiv.id = "sidebar";
    mainDiv.insertAdjacentElement('beforeend', document.createElement('div')).setAttribute("class", "docs-brand");
    mainDiv.lastChild.insertAdjacentHTML('beforeend', `<a class="docs-logo" href="${""}"><img src="${"ressources/Eleca logo.svg"}" alt="Eleca app"><h3 class="title is-3">Eleca</h3></a>`);
    mainDiv.insertAdjacentElement('beforeend', document.createElement("div")).setAttribute("class", "docs-nav");
    for (let cat of items) {
        console.log(mainDiv.getElementsByClassName("docs-nav"))
        mainDiv.getElementsByClassName("docs-nav")[0].insertAdjacentHTML('beforeend', `<p class="menu-label">${cat.title}</p><ul class="menu-list"></ul>`);
        for (let itm of cat.element) {
            mainDiv.getElementsByClassName("menu-list")[mainDiv.getElementsByClassName("menu-list").length - 1].insertAdjacentHTML('beforeend', `<li><a href=${itm.link}>${itm.title}</a></li>`)
        }
    }
    return mainDiv;
}

function HTML_createVoltageDividerResultTable(Results, targetRatio) {
    let mainTable = document.createElement("table");
    mainTable.className = "table is-fullwidth is-hoverable";

    mainTable.appendChild(document.createElement("thead"));
    mainTable.lastChild.appendChild(document.createElement("tr"));
    let mainTable_headerRow = mainTable.lastChild;
    mainTable_headerRow.appendChild(document.createElement("th"));
    mainTable_headerRow.lastChild.insertAdjacentHTML('afterbegin', "GroupId");
    mainTable_headerRow.appendChild(document.createElement("th"));
    mainTable_headerRow.lastChild.insertAdjacentHTML('afterbegin', "Best Score");
    mainTable_headerRow.appendChild(document.createElement("th"));
    mainTable_headerRow.lastChild.insertAdjacentHTML('afterbegin', "Best Error");
    mainTable_headerRow.appendChild(document.createElement("th"));
    mainTable_headerRow.lastChild.insertAdjacentHTML('afterbegin', "Nb of sets");

    for (let group of Results) {
        mainTable.insertAdjacentElement("beforeend", document.createElement("tbody")).setAttribute("class", "table is-fullwidth is-hoverable");
        mainTable.lastChild.insertAdjacentHTML("beforeend", `<th>${group.originId}</th><td>${group.bestScore}</td><td>${(1-(group.bestScore+targetRatio)/targetRatio) + " %"}</td><td>${group.results.length}</td>`);
        mainTable.insertAdjacentElement("beforeend", document.createElement("thead"));
        mainTable.lastChild.insertAdjacentHTML("beforeend", '<tr><th></th><th>Score</th><th>Error</th><th>Variables</th></tr>');
        mainTable.insertAdjacentElement("beforeend", document.createElement("tbody"));
        for (let res of group.results) {
            mainTable.lastChild.insertAdjacentHTML("beforeend", `<tr><td></td><td>${res.score}</td><td>${(1-(res.score+targetRatio)/targetRatio) + " %"}</td><td><span class="icon is-small"><i class=" fas fa-caret-right"></i></span></td></tr>`)
            for (let vr of res.variables) {
                mainTable.lastChild.insertAdjacentHTML("beforeend", `<tr><th>${vr.id}</th><td>${vr.type}</td><td><figure class=" image is-24x24 "><img src="ressources/icons/${vr.type}.svg "></figure></td><th>${vr.getFormattedValue()}</th></tr>`)
            }

        }
    }

    return mainTable
}