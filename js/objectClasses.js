let seriesE3 = [100, 220, 470];
let seriesE6 = [100, 150, 220, 330, 470, 680];
let seriesE12 = [100, 120, 150, 180, 220, 270, 330, 390, 470, 560, 680, 820];
let seriesE24 = [100, 110, 120, 130, 150, 160, 180, 200, 220, 240, 270, 300, 330, 360, 390, 430, 470, 510, 560, 620, 680, 750, 820, 910];
let seriesE48 = [100, 105, 110, 115, 121, 127, 133, 140, 147, 154, 162, 169, 178, 187, 196, 205, 215, 226, 237, 249, 261, 274, 287, 301, 316, 332, 348, 365, 383, 402, 422, 442, 464, 487, 511, 536, 562, 590, 619, 649, 681, 715, 750, 787, 825, 866, 909, 953];
let seriesE96 = [100, 102, 105, 107, 110, 113, 115, 118, 121, 124, 127, 130, 133, 137, 140, 143, 147, 150, 154, 158, 162, 165, 169, 174, 178, 182, 187, 191, 196, 200, 205, 210, 215, 221, 226, 232, 237, 243, 249, 255, 261, 267, 274, 280, 287, 294, 301, 309, 316, 324, 332, 340, 348, 357, 365, 374, 383, 392, 402, 412, 422, 432, 442, 453, 464, 475, 487, 499, 511, 523, 536, 549, 562, 576, 590, 604, 619, 634, 649, 665, 681, 698, 715, 732, 750, 768, 787, 806, 825, 845, 866, 887, 909, 931, 953, 976];
let seriesE192 = [100, 101, 102, 104, 105, 106, 107, 109, 110, 111, 113, 114, 115, 117, 118, 120, 121, 123, 124, 126, 127, 129, 130, 132, 133, 135, 137, 138, 140, 142, 143, 145, 147, 149, 150, 152, 154, 156, 158, 160, 162, 164, 165, 167, 169, 172, 174, 176, 178, 180, 182, 184, 187, 189, 191, 193, 196, 198, 200, 203, 205, 208, 210, 213, 215, 218, 221, 223, 226, 229, 232, 234, 237, 240, 243, 246, 249, 252, 255, 258, 261, 264, 267, 271, 274, 277, 280, 284, 287, 291, 294, 298, 301, 305, 309, 312, 316, 320, 324, 328, 332, 336, 340, 344, 348, 352, 357, 361, 365, 370, 374, 379, 383, 388, 392, 397, 402, 407, 412, 417, 422, 427, 432, 437, 442, 448, 453, 459, 464, 470, 475, 481, 487, 493, 499, 505, 511, 517, 523, 530, 536, 542, 549, 556, 562, 569, 576, 583, 590, 597, 604, 612, 619, 626, 634, 642, 649, 657, 665, 673, 681, 690, 698, 706, 715, 723, 732, 741, 750, 759, 768, 777, 787, 796, 806, 816, 825, 835, 845, 856, 866, 876, 887, 898, 909, 920, 931, 942, 953, 965, 976, 988];

const MAX_ORIGINTABLE_LENGHT = 10;
const MAX_RESULTTABLE_LENGHT = 10;

class VariableObject {
    constructor(id, type = undefined, value = null, possibleValues = []) {
        this.id = id;
        this.type = type;
        this.value = value;
        this.possibleValues = possibleValues;
    }
    copy() {
        let output = new VariableObject(this.id, this.type);
        output.value = new PassiveComponentValue(this.value.value, this.value.origin);
        for (let pv of this.possibleValues) {
            output.possibleValues.push(new PassiveComponentValue(pv.value, pv.origin));
        }
        return output;
    }
    addPossibleValues_ESeries(series) {
        if (this.type === "Resistor") {
            for (let v of series) {
                for (let i = 0.01; i <= 1e9; i *= 10) {
                    this.possibleValues.push(new PassiveComponentValue(v * i, v));
                }
            }
        } else if (this.type === "Capacitor" || this.type === "Inductor") {
            for (let v of series) {
                for (let i = 1e-14; i <= 1e-3; i = i * 10) {
                    this.possibleValues.push(new PassiveComponentValue(v * i, v));
                }
            }
        }
    }
    getFormattedValue() {
        if (this.type === "Resistor") {
            if (this.value.value >= 1000000000) {
                return (Number.parseFloat(this.value.value / 1000000000).toPrecision(3) + " GΩ")
            } else if (this.value.value >= 1000000) {
                return (Number.parseFloat(this.value.value / 1000000).toPrecision(3) + " MΩ")
            } else if (this.value.value >= 1000) {
                return (Number.parseFloat(this.value.value / 1000).toPrecision(3) + " KΩ")
            } else {
                return (Number.parseFloat(this.value.value).toPrecision(3) + " Ω")
            }
        } else if (this.type === "Capacitor") {
            if (this.value.value >= 1e-3) {
                return (Number.parseFloat(this.value.value * 100).toPrecision(3) + "mF")
            } else if (this.value.value >= 1e-6) {
                return (Number.parseFloat(this.value.value * 100000).toPrecision(3) + "µF")
            } else if (this.value.value >= 1e-9) {
                return (Number.parseFloat(this.value.value * 100000000).toPrecision(3) + "nF")
            } else if (this.value.value >= 1e-12) {
                return (Number.parseFloat(this.value.value * 100000000000).toPrecision(3) + "pF")
            } else {
                return (Number.parseFloat(this.value.value).toPrecision(3) + "F")
            }
        } else if (this.type === "Inductor") {
            if (this.value.value >= 1e-3) {
                return (Number.parseFloat(this.value.value * 100).toPrecision(3) + "mH")
            } else if (this.value.value >= 1e-6) {
                return (Number.parseFloat(this.value.value * 100000).toPrecision(3) + "µH")
            } else if (this.value.value >= 1e-9) {
                return (Number.parseFloat(this.value.value * 100000000).toPrecision(3) + "nH")
            } else if (this.value.value >= 1e-12) {
                return (Number.parseFloat(this.value.value * 100000000000).toPrecision(3) + "pH")
            } else {
                return (Number.parseFloat(this.value.value).toPrecision(3) + "H")
            }
        }
    }
}

class PassiveComponentValue {
    constructor(value, origin) {
        this.value = value;
        this.origin = origin;
    }
    copy() {
        return new PassiveComponentValue(this.value, this.origin);
    }
    newEmpty() {
        return new PassiveComponentValue(null, null);
    }
}

class ResultEntry {
    constructor(expression = "", score = Infinity, variables = [], originId = "") {
        this.score = score;
        this.variables = variables;

        if (typeof expression !== "string") {
            if (expression !== undefined || variables !== undefined) {
                let vrs = expression.variables();
                for (let v of vrs) {
                    for (let src of variables) {
                        if (v === src.id) {
                            this.originId += src.value.origin;
                            break;
                        }
                    }
                }
            }
        } else if (variables.every(v => v instanceof VariableObject)) {
            this.originId = String("")
            for (let v of variables) {
                this.originId += v.value.origin.toString();
            }
        }
    }
    copy() {
        let output = new ResultEntry();
        output.score = this.score;
        if (this.variables.every(v => v instanceof VariableObject)) {
            for (let v of this.variables) {
                output.variables.push(v.copy())
            }
        } else {
            output.variables = this.variables
        }
        output.originId = this.originId;
        return output;
    }
}

class ResultOriginTable {
    constructor(originId, bestScore, worstScore, results = []) {
        this.originId = originId;
        this.bestScore = bestScore;
        this.worstScore = worstScore;
        this.results = results;
    }
}

class ResultTable extends Array {
    constructor() {
        super()
        this.tableHasChanged = false;
        this.resultTableWorstScore = Infinity;
        this.hasCorrespondingSourceTable = false;
    }
    static fromJSON(input) {
        let output = new ResultTable();
        let json_object = JSON.parse(input);
        let groupIndex = 0;
        let resultEntryIndex = 0;
        let varsIndex = 0;

        for (let group of json_object) {
            output.push(new ResultOriginTable(group.originId, group.bestScore, group.worstScore))
            resultEntryIndex = 0;
            for (let res of group.results) {
                output[groupIndex].results.push(new ResultEntry("", res.score, [], res.originId));
                varsIndex = 0;
                for (let v of res.variables) {
                    output[groupIndex].results[resultEntryIndex].variables.push(new VariableObject(v.id, v.type, null, []));
                    output[groupIndex].results[resultEntryIndex].variables[varsIndex].value = new PassiveComponentValue(v.value.value, v.value.origin);
                    for (let pv of v.possibleValues) {
                        output[groupIndex].results[resultEntryIndex].variables[varsIndex].possibleValues.push(new PassiveComponentValue(pv.value, pv.originId));
                    }
                    varsIndex++;
                }
                resultEntryIndex++;
            }
            groupIndex++;
        }
        output.resultTableWorstScore = output[0].worstScore;
        for (let table of output) {
            if (table.worstScore > output.resultTableWorstScore) {
                output.resultTableWorstScore = table.worstScore;
            }
        }
        return output;
    }
    updateResultTable(resultEntry) {
        // resultEntry.score <= this.resultTableWorstScore
        if (true) {
            this.hasCorrespondingSourceTable = false;
            this.tableHasChanged = false;
            let i = 0;
            let k = 0;
            for (i = 0; i < this.length; i++) {
                if (resultEntry.originId === this[i].originId) {
                    this.hasCorrespondingSourceTable = true;
                    break;
                }
            };
            if (this.hasCorrespondingSourceTable === false) {
                if (this.length < MAX_RESULTTABLE_LENGHT) {
                    this.push(new ResultOriginTable(resultEntry.originId, resultEntry.score, resultEntry.score, [resultEntry.copy()]));
                    this.tableHasChanged = true;
                } else {
                    for (let j = 0; j < this.length; j++) {
                        if (resultEntry.score < this[j].bestScore) {
                            this.splice(j, 0, new ResultOriginTable(resultEntry.originId, resultEntry.score, resultEntry.score, [resultEntry.copy()]));
                            this.tableHasChanged = true;
                            if (this.length > MAX_RESULTTABLE_LENGHT) {
                                this.length = MAX_RESULTTABLE_LENGHT;
                            }
                            break;
                        }
                    }
                }
            } else {

                // new result have corresponding Origin Table
                for (k = 0; k < this[i].results.length; k++) {
                    if (resultEntry.score < this[i].results[k].score) {
                        this[i].results.splice(k, 0, resultEntry.copy());
                        this.tableHasChanged = true;
                        if (this[i].results.length > MAX_ORIGINTABLE_LENGHT) {
                            this[i].results.length = MAX_ORIGINTABLE_LENGHT;
                        }
                        if (resultEntry.score < this[i].bestScore) {
                            this[i].bestScore = resultEntry.score;
                        } else if (k === this[i].results.length - 1) {
                            this[i].worstScore = resultEntry.score;
                        }
                        break;
                    }
                }
                k++;
                if (k < MAX_ORIGINTABLE_LENGHT && this.tableHasChanged === false) {
                    this[i].results.push(resultEntry.copy());
                    this.tableHasChanged = true;
                    if (resultEntry.score < this[i].bestScore) {
                        this[i].bestScore = resultEntry.score;
                    } else if (k === this[i].results.length - 2) {
                        this[i].worstScore = resultEntry.score;
                    }
                }

            }
        }
        if (this.tableHasChanged === true) {
            this.resultTableWorstScore = this[0].worstScore;
            for (let table of this) {
                if (table.worstScore > this.resultTableWorstScore) {
                    this.resultTableWorstScore = table.worstScore;
                }
            }
            // sorting the subTable by bestScore(then by worstScore if bestScore are equal)
            this.sort((a, b) => (a.bestScore > b.bestScore) ? 1 : (a.bestScore === b.bestScore) ? ((a.worstScore > b.worstScore) ? 1 : -1) : -1);
        }
    }
}

function computeVoltageDivider(R1, R2, targetRatio) {
    let currentResult;
    let resultTable = new ResultTable();
    for (val1 of R1.possibleValues) {
        R1.value = val1
        for (val2 of R2.possibleValues) {
            R2.value = val2;
            currentResult = (R2.value.value) / (R2.value.value + R1.value.value);
            resultTable.updateResultTable(new ResultEntry("", Math.abs(currentResult - targetRatio), [R1, R2]))
        }
    }
    return resultTable;
}