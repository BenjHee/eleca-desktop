function HTML_createVoltageDividerResultTable(Results) {
    let mainTable = document.createElement("table");
    mainTable.className = "table is-fullwidth is-hoverable";

    mainTable.appendChild(document.createElement("thead"));
    mainTable.lastChild.appendChild(document.createElement("tr"));
    let mainTable_headerRow = mainTable.lastChild;
    mainTable_headerRow.appendChild(document.createElement("th"));
    mainTable_headerRow.lastChild.insertAdjacentHTML('afterbegin', "GroupId");
    mainTable_headerRow.appendChild(document.createElement("th"));
    mainTable_headerRow.lastChild.insertAdjacentHTML('afterbegin', "Best Score");
    mainTable_headerRow.appendChild(document.createElement("th"));
    mainTable_headerRow.lastChild.insertAdjacentHTML('afterbegin', "Best Error");
    mainTable_headerRow.appendChild(document.createElement("th"));
    mainTable_headerRow.lastChild.insertAdjacentHTML('afterbegin', "Nb of sets");

    for (let group of Results) {
        mainTable.insertAdjacentElement("beforeend", document.createElement("tbody")).setAttribute("class", "table is-fullwidth is-hoverable");
        mainTable.lastChild.insertAdjacentHTML("beforeend", `<th>${group.originId}</th><td>${group.bestScore}</td><td>${"xxx"}</td><td>${group.results.length}</td>`);
        mainTable.insertAdjacentElement("beforeend", document.createElement("thead"));
        mainTable.lastChild.insertAdjacentHTML("beforeend", '<tr><th></th><th>Score</th><th>Error</th><th>Variables</th></tr>');
        mainTable.insertAdjacentElement("beforeend", document.createElement("tbody"));
        for (let res of group.results) {
            mainTable.lastChild.insertAdjacentHTML("beforeend", `<tr><td></td><td>${res.score}</td><td>${res.score}</td><td><span class="icon is-small"><i class=" fas fa-caret-right"></i></span></td></tr>`)
            for (let vr of res.variables) {
                mainTable.lastChild.insertAdjacentHTML("beforeend", `<tr><th>${vr.id}</th><td>${vr.type}</td><td><figure class=" image is-24x24 "><img src="ressources/icons/${vr.type}.svg "></figure></td><th>${vr.getFormattedValue()}</th></tr>`)
            }
            //console.log(groupTables.lastChild)
            //groupTables.lastChild.insertAdjacentHTML("beforeend", `<td>${group.originId}</td><td>${group.bestScore}</td><td>${"xxx"}</td><td>${group.results.length}</td>`);
            //groupTables.lastChild.insertAdjacentHTML("afterbegin", `<tr class="is-selected"><td>03</td><td>12700</td><td>1.2%</td><td><span class="icon is-small"><i class="fas fa-caret-down"></i></span></td></tr>`)
        }
    }

    return mainTable
}