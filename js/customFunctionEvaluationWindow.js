const { BrowserWindow } = require('electron').remote;
const ipcRenderer = require('electron').ipcRenderer;
const path = require('path');

let scoreString = document.getElementById("scoreString");
let progressionString = document.getElementById("progressionString");
let evaluationReport;


const windowID = BrowserWindow.getFocusedWindow().id;

const invisiblePath = path.join('file://', __dirname, 'inv_customFunctionComputing.html');
let win = new BrowserWindow({
    width: 400,
    height: 400,
    show: true,
    webPreferences: {
        nodeIntegration: true
    }
})
win.loadURL(invisiblePath);


const invisiblePath_updater = path.join('file://', __dirname, 'inv_customFunctionComputing_updateProgression.html');
let win_updater = new BrowserWindow({
    width: 400,
    height: 400,
    show: true,
    webPreferences: {
        nodeIntegration: true
    }
})
win_updater.loadURL(invisiblePath_updater);



win.webContents.on('did-finish-load', () => {
    win.webContents.send('evaluateOperation', JSON.parse(sessionStorage.getItem('variablesList')), sessionStorage.getItem('leftExpression'), sessionStorage.getItem('rightExpression'), windowID);
    //updating evaluation title string
    document.getElementById("equationString").innerHTML = sessionStorage.getItem('leftExpression') + " = " + sessionStorage.getItem('rightExpression')
})
win.on('close', () => {
    win_updater.close();
})

win_updater.webContents.on('did-finish-load', () => {
    win_updater.webContents.send('cf_progressUpdater', windowID)
})

ipcRenderer.on('updateProgression', (event, score, operationCount, totalOperationCount) => {
    scoreString.innerHTML = score;
    progressionString.innerHTML = ((operationCount / totalOperationCount) * 100) + "%";
})

ipcRenderer.on('cf_updateProgress', (event, evaluationReportJSONString) => {
    evaluationReport = JSON.parse(evaluationReportJSONString);
    scoreString.innerHTML = evaluationReport.bestScore;
    progressionString.innerHTML = ((evaluationReport.count / evaluationReport.totalCount) * 100);
})
ipcRenderer.on('EvaluationCompleted', (event) => {
    evaluationReport = ResultTable.fromJSON(localStorage.getItem('customFunctionEvaluation_report'));
    //evaluationReport = JSON.parse(localStorage.getItem('customFunctionEvaluation_report'));
    let dbg_result = JSON.parse(JSON.stringify(evaluationReport));

    scoreString.innerHTML = evaluationReport[0].bestScore;
    progressionString.innerHTML = "100 %";

    let resultTableDiv = document.getElementById("ResultTableDiv");

    resultTableDiv.insertAdjacentElement("afterbegin", HTML_createResultTable(evaluationReport));
})