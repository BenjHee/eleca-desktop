const ipc = require('electron').ipcRenderer;
const BrowserWindow = require('electron').remote.BrowserWindow;

var Parser = require('expr-eval').Parser;

let leftEq;
let rightEq;
let equation;
let nbOfOperations = 0;
let operationCount = 0;
let fromWindow;
let DBG_t0 = 0;
let DBG_t1 = 0;
let DBG_perfMean = 0;

var parser = new Parser({
    operators: {
        add: true,
        concatenate: true,
        conditional: true,
        divide: true,
        factorial: true,
        multiply: true,
        power: true,
        remainder: true,
        subtract: true,

        // Disable and, or, not, <, ==, !=, etc.
        logical: false,
        comparison: false,

        // Disable 'in' and = operators
        'in': false,
        assignment: false
    }
});

function operation(variableObjects, varindex) {
    for (let v of variableObjects) {
        v.value = v.possibleValues[0];
    }
    for (let j = 0; j < variableObjects[varindex].possibleValues.length; j++) {
        // changing the current variable value;
        variableObjects[varindex].value = variableObjects[varindex].possibleValues[j];

        // applying variables values to the object used by the parser
        equationVrsObj = {};
        for (let v of variableObjects) {
            equationVrsObj[`${v.id}`] = v.value.value;
        }
        // computing the result
        result = equation.evaluate(equationVrsObj);
        operationCount++;
        DBG_t0 = performance.now();
        sessionStorage.setItem('customFunctionEvaluation_report', JSON.stringify({
            score: result,
            count: operationCount,
            totalCount: nbOfOperations
        }));
        //fromWindow.webContents.send('updateProgression', Math.abs(result), operationCount, nbOfOperations);
        DBG_t1 = performance.now();
        DBG_perfMean += ((DBG_t1 - DBG_t0) / 1000);
        if (varindex < variableObjects.length - 1) {
            operation(variableObjects, varindex + 1)
        }

    }
}

function evaluateExpression(leftExpression, rightExpression, variableObjects) {

    equation = Parser.parse("(" + leftExpression + ") - (" + rightExpression + ")");
    let vrsValIndex = new Array(variableObjects.length);
    let equationVrsObj;
    let result;
    let score;

    // counting the number of operation necessary
    for (let i = 0; i < variableObjects.length; i++) {
        let tmp = 1;
        for (let j = 0; j < variableObjects.length - i; j++) {
            tmp *= variableObjects[j].possibleValues.length;
        }
        nbOfOperations += tmp;
    }
    console.log("nb of operations :" + nbOfOperations);

    operation(variableObjects, 0);
    console.log(operationCount);
    document.body.innerHTML = operationCount;
}
// :::::::::::::::::::::::::::::::::::::::::

//console.log(Parser.parse("1/0").evaluate());
ipc.on('evaluateOperation', function(event, variableList, leftExpression, rightExpression, fromWindowId) {
    fromWindow = BrowserWindow.fromId(fromWindowId);
    let t0 = performance.now();
    evaluateExpression(leftExpression, rightExpression, variableList);
    let t1 = performance.now();
    document.body.innerHTML += "Computing time : " + (t1 - t0) + " ms";
    document.body.innerHTML += "    ipc send performance: " + (DBG_perfMean / nbOfOperations) * 1000 + " ms";
    //window.close()
})