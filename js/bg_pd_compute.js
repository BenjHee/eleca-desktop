onmessage = function(e) {
    let resultTable = computeVoltageDivider(
        new VariableObject("R1", params.variableObjectTemplate.type, null, params.variableObjectTemplate.possibleValues),
        new VariableObject("R2", params.variableObjectTemplate.type, null, params.variableObjectTemplate.possibleValues),
        params.ratio);
    postMessage(resultTable);
}