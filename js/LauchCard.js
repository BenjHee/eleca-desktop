const { BrowserWindow } = require('electron').remote
const electron = require('electron');
const { ipcRenderer } = electron;
const path = require('path');
const shell = require('electron').shell

const links = document.querySelectorAll('a[href]')


const customFunctionWindowBtn = document.getElementById('LaunchCustomFunctionBtn');
const voltageDividerWindowBtn = document.getElementById('LaunchVoltageDividerBtn');

// opening all http/https links externally
Array.prototype.forEach.call(links, (link) => {
    const url = link.getAttribute('href')
    if (url.indexOf('http') === 0) {
        link.addEventListener('click', (e) => {
            e.preventDefault()
            shell.openExternal(url)
        })
    }
})

let winList = BrowserWindow.getAllWindows();

function autoCloseLaunchCard(e) {
    e.preventDefault;
    ipcRenderer.send('closeLaunchCard');
}

customFunctionWindowBtn.addEventListener('click', function(e) {
    const modalPath = path.join('file://', __dirname, 'customFunction.html')
    let win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    })

    win.on('ready', () => {})
    win.on('close', () => { win = null })
    win.loadURL(modalPath)
    win.show()
    autoCloseLaunchCard(e);
});

voltageDividerWindowBtn.addEventListener('click', function(e) {
    const modalPath = path.join('file://', __dirname, 'pontDiviseur.html')
    let win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    })

    win.on('ready', () => {})
    win.on('close', () => { win = null })
    win.loadURL(modalPath)
    win.show()
    autoCloseLaunchCard(e);
});