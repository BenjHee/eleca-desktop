function html_addNewVariablesOptions(variableName) {
    let div0 = document.createElement("div");
    div0.className = "column";


    let div1 = document.createElement("div");
    div1.className = "card";
    div1.id = variableName + "_card";


    let div2 = document.createElement("div");
    div2.className = "card-content";
    let p_title = document.createElement("p");
    p_title.className = "title is-1";
    p_title.innerHTML = variableName;


    let div3 = document.createElement("div");
    div3.className = "control has-icons-left";
    let p_typeTitle = document.createElement("p");
    p_typeTitle.className = "title is-6";
    p_typeTitle.innerHTML = "Type";


    let div4 = document.createElement("div");
    div4.className = "select";
    let select_type = document.createElement("select");
    select_type.id = variableName + "_select"
    let option_resistance = document.createElement("option");
    option_resistance.innerHTML = "Resistor";
    option_resistance.value = "r";
    let option_capacitor = document.createElement("option");
    option_capacitor.innerHTML = "Capacitor";
    option_capacitor.value = "c";
    let option_inductor = document.createElement("option");
    option_inductor.innerHTML = "Inductor";
    option_inductor.value = "l";

    let div5 = document.createElement("div");
    div5.className = "icon is-small is-left"
    let i_icon = document.createElement("i");
    i_icon.className = "fas fa-globe"

    let div6 = document.createElement("div");
    let p_valuesTitle = document.createElement("p");
    p_valuesTitle.className = "title is-6";
    p_valuesTitle.innerHTML = "Values:";

    let label_checkboxE3 = document.createElement("label");
    label_checkboxE3.className = "checkbox";
    label_checkboxE3.innerHTML = "E3";
    let input_checkboxE3 = document.createElement("input");
    input_checkboxE3.type = "checkbox";
    input_checkboxE3.id = variableName + "_E3CheckBox";

    let label_checkboxE6 = document.createElement("label");
    label_checkboxE6.className = "checkbox";
    label_checkboxE6.innerHTML = "E6";
    let input_checkboxE6 = document.createElement("input");
    input_checkboxE6.type = "checkbox";
    input_checkboxE6.id = variableName + "_E6CheckBox";

    let label_checkboxE12 = document.createElement("label");
    label_checkboxE12.className = "checkbox";
    label_checkboxE12.innerHTML = "E12";
    let input_checkboxE12 = document.createElement("input");
    input_checkboxE12.type = "checkbox";
    input_checkboxE12.id = variableName + "_E12CheckBox";

    let label_checkboxE24 = document.createElement("label");
    label_checkboxE24.className = "checkbox";
    label_checkboxE24.innerHTML = "E24";
    let input_checkboxE24 = document.createElement("input");
    input_checkboxE24.type = "checkbox";
    input_checkboxE24.id = variableName + "_E24CheckBox";

    let label_checkboxE48 = document.createElement("label");
    label_checkboxE48.className = "checkbox";
    label_checkboxE48.innerHTML = "E48";
    let input_checkboxE48 = document.createElement("input");
    input_checkboxE48.type = "checkbox";
    input_checkboxE48.id = variableName + '_E48CheckBox';


    let label_checkboxE96 = document.createElement("label");
    label_checkboxE96.className = "checkbox";
    label_checkboxE96.innerHTML = "E96";
    let input_checkboxE96 = document.createElement("input");
    input_checkboxE96.type = "checkbox";
    input_checkboxE96.id = variableName + '_E96CheckBox';

    let label_checkboxE192 = document.createElement("label");
    label_checkboxE192.className = "checkbox";
    label_checkboxE192.innerHTML = "E192";
    let input_checkboxE192 = document.createElement("input");
    input_checkboxE192.type = "checkbox";
    input_checkboxE192.id = variableName + "_E192CheckBox";
    // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    label_checkboxE192.appendChild(input_checkboxE192);
    label_checkboxE96.appendChild(input_checkboxE96);
    label_checkboxE48.appendChild(input_checkboxE48);
    label_checkboxE24.appendChild(input_checkboxE24);
    label_checkboxE12.appendChild(input_checkboxE12);
    label_checkboxE6.appendChild(input_checkboxE6);
    label_checkboxE3.appendChild(input_checkboxE3);

    div6.appendChild(p_valuesTitle);
    div6.appendChild(label_checkboxE3);
    div6.appendChild(label_checkboxE6);
    div6.appendChild(label_checkboxE12);
    div6.appendChild(label_checkboxE24);
    div6.appendChild(label_checkboxE48);
    div6.appendChild(label_checkboxE96);
    div6.appendChild(label_checkboxE192);

    div5.appendChild(i_icon);

    select_type.appendChild(option_resistance);
    select_type.appendChild(option_capacitor);
    select_type.appendChild(option_inductor);
    div4.appendChild(select_type);

    div3.appendChild(p_typeTitle);
    div3.appendChild(div4);
    div3.appendChild(div5);

    div2.appendChild(p_title);
    div2.appendChild(div3);
    div2.appendChild(div6);

    div1.appendChild(div2);

    div0.appendChild(div1);
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    document.getElementById("variablesListField").appendChild(div0);

};