const { BrowserWindow } = require('electron');
const path = require('path');
const Parser = require('expr-eval').Parser;

let leftExpressionStringField = document.getElementById('leftExpressionInput');
let rightExpressionStringField = document.getElementById('rightExpressionInput');
let evaluateExpressionButton = document.getElementById('evaluateButton');
let validateExpressionButton = document.getElementById('validateButton');

let variables;
let variablesList = [];
let i = 0;
let j = 0;


let validExpression = false;
evaluateExpressionButton.disabled = true;

leftExpressionStringField.addEventListener('change', () => {
    variables = parseVariables();
    updatevariableObjectsList();
    validateExpression();
})
rightExpressionStringField.addEventListener('change', () => {
    variables = parseVariables();
    updatevariableObjectsList();
    validateExpression();
})

validateExpressionButton.addEventListener('click', () => {
    validateExpression();
});

evaluateExpressionButton.addEventListener('click', () => {
    assignValueInVariables();
    sessionStorage.setItem("variablesList", JSON.stringify(variablesList));
    sessionStorage.setItem("leftExpression", leftExpressionStringField.value);
    sessionStorage.setItem("rightExpression", rightExpressionStringField.value);
    const modalPath = path.join('file://', __dirname, 'customFunctionComputing.html')
    window.location.replace(modalPath);

})

function onChangeHandlingFunction() {
    variables = parseVariables();
    updatevariableObjectsList();
    validateExpression();
}

function parseVariables() {
    /*
     *   Match in the expression every single "meaninglessCharacter"
     *   preceded by nothing or one or more non-"meaninglessCharacter"
     *   and followed by nothing or one or more non-"meaninglessCharacter"
     * 
     *   ("meaninglessCharacter" beiing something else than a operator,
     *   a number or structural character : +-=/*%^()[].0123456789*space*tabs*newline)
     */
    let regex = /((?<=[^a-z])|(?<=^))[a-zA-Z]((?=[^a-z])|(?=$))/g;
    let leftExpr = leftExpressionStringField.value;
    let rightExpr = rightExpressionStringField.value;
    let var_L = leftExpr.match(regex);
    let var_R = rightExpr.match(regex);
    let vars = [];
    if (var_L !== null) { vars = vars.concat(var_L) };
    if (var_R !== null) { vars = vars.concat(var_R) };
    // deleting multiple occurences of the same variable
    if (vars.length > 1) {
        i = vars.length - 1;
        let isPresent = false;
        while (i > 0) {
            for (j = i - 1; j >= 0; j--) {
                if (vars[j] === vars[i]) {
                    isPresent = true;
                    break;
                }
            }
            if (isPresent === true) {
                vars.splice(j, 1);
            }
            i--;
            isPresent = false
        }
    }
    return vars;
}

function updatevariableObjectsList() {
    variablesList = [];
    for (let i = 0; i < variables.length; i++) {
        variablesList.push(new VariableObject(variables[i], undefined, null, []));
    }

    document.getElementById("variablesListField").innerHTML = ''
    for (let i = 0; i < variablesList.length; i++) {
        html_addNewVariablesOptions(variablesList[i].id);
    }
}

function validateExpression() {
    let allVariablesAreValid = true;
    if (variables.length > 0) {
        for (let i = 0; i < variablesList.length; i++) {
            let typeSelect = document.getElementById(variablesList[i].id + "_select")

            if ((typeSelect.value !== "r" && typeSelect.value !== "l" && typeSelect.value !== "c") ||
                (document.getElementById(variablesList[i].id + "_E3CheckBox").checked === false &&
                    document.getElementById(variablesList[i].id + "_E6CheckBox").checked === false &&
                    document.getElementById(variablesList[i].id + "_E12CheckBox").checked === false &&
                    document.getElementById(variablesList[i].id + "_E24CheckBox").checked === false &&
                    document.getElementById(variablesList[i].id + "_E48CheckBox").checked === false &&
                    document.getElementById(variablesList[i].id + "_E96CheckBox").checked === false &&
                    document.getElementById(variablesList[i].id + "_E192CheckBox").checked === false)) {
                allVariablesAreValid = false;
            }
        }
    } else {
        allVariablesAreValid = false;
    }
    // :::::::::::::::::::::::::::::::::::::::::::::::::::::::
    if (allVariablesAreValid) {
        evaluateExpressionButton.disabled = false;
        return true;
    } else {
        evaluateExpressionButton.disabled = true;
        return false;
    };
}

function assignValueInVariables() {

    let E3 = false;
    let E6 = false;
    let E12 = false;
    let E24 = false;
    let E48 = false;
    let E96 = false;
    let E192 = false;

    for (let i = 0; i < variablesList.length; i++) {

        switch (document.getElementById(variablesList[i].id + "_select").value) {
            case "r":
                variablesList[i].type = "Resistor";
                break;
            case "l":
                variablesList[i].type = "Capacitor";
                break;
            case "c":
                variablesList[i].type = "Inductor";
                break;
        }

        E3 = document.getElementById(variablesList[i].id + "_E3CheckBox").checked;
        E6 = document.getElementById(variablesList[i].id + "_E6CheckBox").checked;
        E12 = document.getElementById(variablesList[i].id + "_E12CheckBox").checked;
        E24 = document.getElementById(variablesList[i].id + "_E24CheckBox").checked;
        E48 = document.getElementById(variablesList[i].id + "_E48CheckBox").checked;
        E96 = document.getElementById(variablesList[i].id + "_E96CheckBox").checked;
        E192 = document.getElementById(variablesList[i].id + "_E192CheckBox").checked;

        setPossibleValues(variablesList[i].id, E3, E6, E12, E24, E48, E96, E192);
    }
}

function setPossibleValues(varId, E3, E6, E12, E24, E48, E96, E192) {
    for (let i = 0; i < variablesList.length; i++) {
        if (variablesList[i].id === varId) {
            variablesList[i].possibleValues = [];
            if (E3) { variablesList[i].addPossibleValues_ESeries(seriesE3) };
            if (E6) { variablesList[i].addPossibleValues_ESeries(seriesE6) };
            if (E12) { variablesList[i].addPossibleValues_ESeries(seriesE12) };
            if (E24) { variablesList[i].addPossibleValues_ESeries(seriesE24) };
            if (E48) { variablesList[i].addPossibleValues_ESeries(seriesE48) };
            if (E96) { variablesList[i].addPossibleValues_ESeries(seriesE96) };
            if (E192) { variablesList[i].addPossibleValues_ESeries(seriesE192) };
            variablesList[i].possibleValues = Array.from(new Set(variablesList[i].possibleValues));
            break;
        }
    }
}

function debug() {
    for (vr in variables) {
        console.log("var:" + vr.id + "   type:" + vr.type + "   value:" + vr.value)
    }
}